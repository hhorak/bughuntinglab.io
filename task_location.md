## Where are the tasks located

As seen in the examples of [Submit task](submit_task.md), in the container, the directory **/home/bughunting** contains a directory named **sources**. This directory contains directories with names of the tasks. When you open the directory with one of the task names, you’ll see all the sources needed. **Follow the steps to reproduce a failure that is described in the README.md.**

Please note that the tasks are usually artificial and the solutions are pretty easy. Don’t spend too much time on one task - if you struggle, there are other tasks that can be a better fit for you.

[Back](index.html)
