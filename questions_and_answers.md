## F&Q

1. **Question: I want to start the container and see the following error, what should I do?**

       Error: error creating container storage: the container name "bughunting"
       is already in use by "63e4f8f69068f2121ab7b6ec261b7cb89c51fd68c6b3cc462786d767bcd2dee5".
       You have to remove that container to be able to reuse that name.: that
       name is already in use

    **Answer:** You already have created a container before. Use `docker -ti exec bughunting bash` to attach to that existing container.

2. **Question: I'm getting the following error when I'm pulling the image, what should I do?**

       $ docker run -d --pull always --name bughunting quay.io/bughunting/client
       Emulate Docker CLI using podman. Create /etc/containers/nodocker to quiet msg.
       Trying to pull quay.io/bughunting/client...
         Get https://d3uo42mtx6z2cr.cloudfront.net/sha256/cd/cd4916bc5314ba8615a141133fbe9872dfe2e1dcfbab66e6e2ec34f5ba298121?Expires=1586934124&Signature=bqPl9sRMYa8I3FsI8oe20LDsU8p6jtadMfKS~RiyXoY98IjlA~tf0DPMnJokpApMuu91L14Gg0iBn9yiXVtFXzaJlJNRpf75~ak3MdG1MyTGRLOUK~MM34A1ZVe6mTYZ9sb4cnsEFRr8min5S6isTgQ6q4rRJFF2qJcB1cHJ2YD6s5h0plcrXvYgIwmzSwluutgkIwOpABxVt-ykTVpTt8~HHcBEKBr-FQaZuwHPPJSGbfGwUtMGVgTLb3Pl7hlf-Y~KSf2cvS7sQepxRrZ5rgYtTYMFtlw08eZ3o8yKEgLODSh0D54KRCBOiz3dgkwI9J-gC5igPXB-JBnJWu-qxw__&Key-Pair-Id=APKAJ67PQLWGCSP66DGA: dial tcp: lookup d3uo42mtx6z2cr.cloudfront.net on 127.0.0.1:53: server misbehaving
       Error: unable to pull quay.io/bughunting/client: unable to pull image: Error parsing image configuration: Get https://d3uo42mtx6z2cr.cloudfront.net/sha256/cd/cd4916bc5314ba8615a141133fbe9872dfe2e1dcfbab66e6e2ec34f5ba298121?Expires=1586934124&Signature=bqPl9sRMYa8I3FsI8oe20LDsU8p6jtadMfKS~RiyXoY98IjlA~tf0DPMnJokpApMuu91L14Gg0iBn9yiXVtFXzaJlJNRpf75~ak3MdG1MyTGRLOUK~MM34A1ZVe6mTYZ9sb4cnsEFRr8min5S6isTgQ6q4rRJFF2qJcB1cHJ2YD6s5h0plcrXvYgIwmzSwluutgkIwOpABxVt-ykTVpTt8~HHcBEKBr-FQaZuwHPPJSGbfGwUtMGVgTLb3Pl7hlf-Y~KSf2cvS7sQepxRrZ5rgYtTYMFtlw08eZ3o8yKEgLODSh0D54KRCBOiz3dgkwI9J-gC5igPXB-JBnJWu-qxw__&Key-Pair-Id=APKAJ67PQLWGCSP66DGA: dial tcp: lookup d3uo42mtx6z2cr.cloudfront.net on 127.0.0.1:53: server misbehaving

    **Answer:** From time to time, there are some issues with quay.io. Just keep trying and the command will eventually succeed.

3. **Question: How do I exit the container?**

    **Answer:** Press <kbd>ctrl</kbd>+<kbd>d</kbd> .

4. **Question: I would like to use gdb (or any other tool for debugging). What should I do?**

    **Answer:** You can install further packages in the container. For example, gdb can be installed using `dnf install gdb`.

5. **Question: I didn't find a solution to my problem here**

    **Answer:** Reach out to us in the Hopin session.

[Back](index.html)
