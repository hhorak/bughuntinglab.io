## Setting up the environment

* If you have any troubles setting up the environment, please reach out to us in the Hopin session.
* We recommend you to use Podman on Linux and MacOS, but Docker should work fine as well.
* If you choose Podman, install `podman-docker` wrapper, which enables you to type `docker` and `podman` interchangably.

_ _ _ _
### Get and run the container image

1. Pull the container:
  ```
    docker pull quay.io/bughunting/client:test
  ```
_ _ _ _
#### (A) With bind mounting

Bind mounting the container to a directory allows to use local IDEs and text editors to complete the task,
because the folder will be available for you locally, as usual.

2. Create and enter a directory that the tasks will be mounted to:
  ```
    mkdir bughunting-tasks-test
    cd bughunting-tasks-test
  ```

3. Run the container
  ```
    docker run -e KEY=1234567890101112 -dt --name bughunting-test -v"${PWD}:/home/bughunting/sources:z,rw" quay.io/bughunting/client:test
  ```
_ _ _ _
#### (B) Without bind mounting

The container can be ran without bind mounting the tasks directory.

2. Run the container
  ```
    docker run -e KEY=1234567890101112 -dt --name bughunting quay.io/bughunting/client:test
  ```

3. Choose your command-line file editor

We recommend `nano`. Please go to (A) if unsure.


_ _ _ _
### Enter the running container

You can enter the container repeatedly any time it's running (and re-run it if it's not).

4. Enter the container:
  ```
    docker exec -ti bughunting-test zsh
  ```

_ _ _ _
### Remove the running container & its image 

When you're finished, you can clean up the running container:

5. Remove the running container
  ```
    docker rm -f bughunting-test
  ```

_And, if you don't intend to run BugHunting again._

6. Remove the downloaded container image
  ```
    docker rmi -f quay.io/bughunting/client:test
  ```

[Back](index.html)
