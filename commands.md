## What the commands on welcome-screen do?

**For reference only. Please use the commands provided in web interface.**

1. Pull the container.
    ```console
    docker pull quay.io/bughunting/client
    ```
    Pulling the image may take some time, for the first time. Subsequent runs will be faster.

2. Prepare an empty working directory, which you will bind-mount into the container:
    ```console
    mkdir bughunting-tasks
    cd bughunting-tasks
    ```
     _And also enter the directory._ The tasks will get installed into this directory.

3. Run the container, bind-mounting the `bughunting-tasks` directory into the container.
    ```console
    docker run -e KEY=???? -dt -v"$(pwd):/home/bughunting/sources:z,rw" --name bughunting quay.io/bughunting/client
    ```
   _While also inserting KEY into the container._

4. Enter the running container:
    ```console
    docker exec -ti bughunting zsh
    ```

[Back](index.html)
