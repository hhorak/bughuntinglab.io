## Solve tasks

**You need to have the container running.**

Now, the following commands are run inside the container:

1. You can list the directories with tasks:
    ```
    [bughunting] $ ls
    warm_up
    ```

2. For working on a task, you need to be inside the task directory
    ```
    [bughunting] $ cd warm_up
    ```

3. Edit the source code

   If you've bind-mounted the tasks, you can edit the task files, as any other on computer. Use your favorite IDE / text editor.
   _We recommend [Notepad++](https://notepad-plus-plus.org/downloads/)._

   As an alternative, you can edit the file either inside the container (Press CTRL+X to exit the `nano` editor):
    ```
    [bughunting] $ nano script.sh
    ```

   Or you can edit the file in your `bughunting-tasks` directory on your host.

4. Then the solution can be submitted for evaluation:
    ```
    [bughunting] $ hunt
    ```

5. You see immediately whether you scored or not:
    ```
    OK: You gain 1 score points. 
    ```

[Back](index.html)
